# Build the thing
FROM golang:1.21-alpine AS builder
LABEL maintainer="c2games"

ENV APP_DIR /tmp/build

WORKDIR $APP_DIR

COPY . $APP_DIR/

RUN go install -ldflags "-s -w" -v ./... && \
    go clean -r -cache -modcache

# Build the actual runtime container
FROM alpine:3.14

ARG NAME="soxprox"
ARG DESCRIPTION="C2Games websocket command connector"
ARG MAINTAINER="c2games.org <support@c2games.org>"
ARG VENDOR="c2games"
ARG ARCH=amd64

# Copy the binary from the build container
COPY --from=builder /go/bin/soxprox /usr/local/bin/soxprox

ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.1/s6-overlay-amd64.tar.gz /tmp/

RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C / \
    && \
    rm /tmp/s6-overlay-amd64.tar.gz

ENTRYPOINT  ["/usr/local/bin/soxprox"]
CMD []

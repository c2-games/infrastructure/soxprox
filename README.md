# Soxprox - it will proxy your sox off

Proxy a command executed to a websocket

## Authentication

Only pre-shared key authentication available right now. Set the http header
`x-soxprox-psk` with your password. Leave it blank for no restrictions.

## Usage

Run it `SOXPROX_PORT=4444 SOXPROX_CMD="/bin/sh" go run .`
Then connect with a client `websocat ws://localhost:4444`

## Configuration

All configuration is done through environment vars:

```sh
SOXPROX_PSK="blah"
SOXPROX_ADDRESS="0.0.0.0"
SOXPROX_PORT="4444"
SOXPROX_CMD="/bin/sh"
SOXPROX_ARGS="-c \"echo hello world\""
```

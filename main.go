package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"os/exec"
	"strings"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

func chanFromConn(conn net.Conn) chan []byte {
	c := make(chan []byte)
	go func() {
		for {
			msg, op, err := wsutil.ReadClientData(conn)
			if err != nil {
				c <- nil
				return
			}
			if !op.IsData() {
				fmt.Println("warning: got non data message")
				continue
			}
			n := len(msg)
			if n > 0 {
				res := make([]byte, n)
				// Copy the buffer so it doesn't get changed while read by the recipient.
				copy(res, msg)
				c <- res
			}
		}
	}()

	return c
}

func sendToChan(r io.ReadCloser, c chan []byte) {
	defer r.Close()
	b := make([]byte, 1024)
	for {
		n, err := r.Read(b)
		if n > 0 {
			res := make([]byte, n)
			// Copy the buffer so it doesn't get changed while read.
			copy(res, b)
			c <- res
		}
		if err != nil {
			c <- nil
			return
		}
	}
}

func chanFromCmd(cmd *exec.Cmd) chan []byte {
	c := make(chan []byte)
	// Attach STDOUT stream
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println(err)
	}

	// Attach STDERR stream
	stderr, err := cmd.StderrPipe()
	if err != nil {
		fmt.Println(err)
	}

	go sendToChan(stdout, c)
	go sendToChan(stderr, c)

	return c
}

func connRunCmd(conn net.Conn, cmdName string, args ...string) {
	defer conn.Close()
	cmd := exec.Command(cmdName, args...)
	webChan := chanFromConn(conn)
	cmdChan := chanFromCmd(cmd)
	stdin, err := cmd.StdinPipe()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer stdin.Close()
	err = cmd.Start()
	fmt.Printf("debug: starting pid: %d\n", cmd.Process.Pid)
	if err != nil {
		return
	}
	go func() {
		err = cmd.Wait()
		cmd = nil
	}()

	for {
		select {
		case webData := <-webChan:
			if webData == nil {
				fmt.Printf("debug: web connection closed: %v\n", conn.RemoteAddr())
				if cmd != nil && cmd.ProcessState.ExitCode() == -1 {
					cmd.Process.Kill()
				}
				return
			}
			stdin.Write(webData)
		case cmdData := <-cmdChan:
			if cmdData == nil {
				fmt.Printf("debug: command finished\n")
				wsutil.WriteServerText(conn, []byte("Connection Terminated"))
				return
			}
			err = wsutil.WriteServerText(conn, cmdData)
			if err != nil {
				fmt.Printf("error: %s\n", err.Error())
				if cmd != nil && cmd.ProcessState.ExitCode() == -1 {
					cmd.Process.Kill()
				}
				return
			}
		}
	}
}

func main() {
	const NAME = "SOXPROX"
	const PREFIX = NAME + "_"
	psk := os.Getenv(PREFIX + "PSK")
	address := os.Getenv(PREFIX + "ADDRESS")
	if address == "" {
		address = "0.0.0.0"
	}
	port := os.Getenv(PREFIX + "PORT")
	if port == "" {
		port = "8080"
	}
	cmd := os.Getenv(PREFIX + "CMD")
	if cmd == "" {
		fmt.Printf("fatal: required %s must be specified\n", PREFIX+"CMD")
		os.Exit(1)
	}
	args := os.Getenv(PREFIX + "ARGS")
	argsSplit := []string{}
	if args != "" {
		r := csv.NewReader(strings.NewReader(args))
		r.Comma = ' '
		v, err := r.Read()
		if err != nil {
			fmt.Printf("fatal: error parsing %s: %s\n", PREFIX+"ARGS", err.Error())
			os.Exit(1)
		}
		argsSplit = v
	}
	fmt.Printf("info: starting on %s\n", address+":"+port)
	err := http.ListenAndServe(
		address+":"+port,
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if psk != "" {
				v := r.Header.Get("x-" + NAME + "-psk")
				if v != psk {
					w.WriteHeader(http.StatusUnauthorized)
					w.Write([]byte("Authentication failure"))
				}
			}
			conn, _, _, err := ws.UpgradeHTTP(r, w)
			if err != nil {
				fmt.Printf("error: %s\n", err.Error())
				return
			}
			go connRunCmd(conn, cmd, argsSplit...)
		}),
	)
	if err != nil {
		fmt.Printf("fatal: server error: %s\n", err.Error())
	}
}
